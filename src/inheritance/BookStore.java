//Etienne Plante 1934877
package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		books[0] = new Book("1984", "George Orwell");
		books[1] = new ElectronicBook("The Stand", "Stephen King", 1503422);
		books[2] = new Book("Dune", "Frank Herbert");
		books[3] = new ElectronicBook("Norwegian Woods", "Murakami Haruki", 15425);
		books[4] = new ElectronicBook("Germinal", "Emile Zola", 185435);
		
		for(Book b : books)
		{
			System.out.println(b);
		}
	}
}
