//Etienne Plante 1934877
package inheritance;

public class ElectronicBook extends Book
{
	private int numberBytes;
	
	public ElectronicBook(String title, String author, int numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}
	
	public String toString() { return getTitle() + " was written by " + getAuthor() + " and contains " + numberBytes + " bytes"; }
}
