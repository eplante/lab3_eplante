//Etienne Plante 1934877
package inheritance;

public class Book {
	protected String title;
	private String author;
	
	public Book(String title, String author)
	{
		this.title = title;
		this.author = author;
	}
	
	public String getTitle() { return title; }
	public String getAuthor() { return author; }
	
	public String toString() { return title + " was written by " + author; }
}
