//Etienne Plante 1934877
package geometry;

public interface Shape {
	double getArea();
	double getPerimeter();
}
