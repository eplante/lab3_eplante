//Etienne Plante 1934877
package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	
	public Rectangle(double length, double width)
	{
		this.length = length;
		this.width = width;
	}
	
	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}
	
	@Override
	public double getArea() {
		return length * width;
	}

	@Override
	public double getPerimeter() {
		return 2 * length + 2 * width;
	}
}
