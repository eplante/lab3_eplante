//Etienne Plante 1934877
package geometry;

public class LotsOfShapes {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(10, 15);
		shapes[1] = new Rectangle(2, 3);
		shapes[2] = new Circle(14.122);
		shapes[3] = new Circle(1);
		shapes[4] = new Square(14.5);
		
		for(Shape s : shapes)
		{
			System.out.println("Area: " + s.getArea() + ", Perimeter: " + s.getPerimeter());
		}
	}
}
